# Resume Builder
Yet another project aimed at building resumes automatically by separating content from style. The approach here is to put all of the content into a .YAML file and use the jinja2 templating engine to produce HTML, PDF, and plain text versions. The PDF is generated using XeLaTeX. 

More details and documentation to follow ...

p.s. 

Thanks to   https://www.pongoresume.com/blogPosts/479/d-ohhhhh-here-s-homer-simpson-s-resume-.cfm for providing an interesting resume to use as a demo!

