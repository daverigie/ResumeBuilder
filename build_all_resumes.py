import yaml
import jinja2 
import os, subprocess, sys, glob
import re

templateLoader = jinja2.FileSystemLoader( searchpath="./templates")
templateEnv = jinja2.Environment( loader=templateLoader, trim_blocks=True, lstrip_blocks=True )

LATEX_SUBS = (
    (re.compile(r'\\'), r'\\textbackslash'),
    (re.compile(r'([{}_#%&$])'), r'\\\1'),
    (re.compile(r'~'), r'\~{}'),
    (re.compile(r'\^'), r'\^{}'),
    (re.compile(r'"'), r"''"),
    (re.compile(r'\.\.\.+'), r'\\ldots'),
    (re.compile(r'&'), r'&'),
)

LATEX_CLEAN_PATTERNS = ('*.aux', '*.log', '*.')

def delete_files(pattern):
    for f in glob.glob(pattern):
        os.remove(f)

def escape_tex(value):
    newval = value
    for pattern, replacement in LATEX_SUBS:
        newval = pattern.sub(replacement, newval)
    return newval

texenv = jinja2.Environment( loader=templateLoader, trim_blocks=True, lstrip_blocks=True )
texenv.block_start_string = '((*'
texenv.block_end_string = '*))'
texenv.variable_start_string = '((('
texenv.variable_end_string = ')))'
texenv.comment_start_string = '((='
texenv.comment_end_string = '=))'
texenv.filters['escape_tex'] = escape_tex

def build_tex(filename):
    filename, ext = os.path.splitext(filename)
    val = subprocess.call('xelatex {}'.format(filename), shell=True)
    for ext in LATEX_CLEAN_PATTERNS:
        delete_files(ext)
    return val
    
def file_ext(filename):
    filename, ext = os.path.splitext(filename)
    return ext


def yaml2dict(filename):
    with open(filename, "r") as stream:
        resume_dict = yaml.load(stream)

    return resume_dict

def render2file(templatefile, filename, *args, **kwargs):
    
    in_base, in_ext = os.path.splitext(templatefile)
    out_base, out_ext = os.path.splitext(filename)
    filename = out_base + in_ext

    if file_ext(templatefile) == '.tex':
        tenv = texenv
    else:
        tenv = templateEnv
            
    template = tenv.get_template(templatefile)
    with open(filename, "w+") as f:
        f.write(template.render(*args, **kwargs))


if __name__ == "__main__":

    if len(sys.argv) == 1:
        print('Usage:')
        print('build_all_resumes content.yaml outputbasename')

    contentfile = sys.argv[1]
    outfile     = sys.argv[2]

    resume_dict = yaml2dict(contentfile)

    for template_file in ('resume.txt', 'resume.html', 'resume.tex'):
        render2file(template_file, outfile, **resume_dict)

    # Build resulting latex file
    build_tex(outfile)



    